<!DOCTYPE HTML>  
<html>
<?php
session_start();
?>

<style>
html, body {
    margin: 0;
    padding: 0;
	background: #333333;
}
#white1 {
	background: white;
	height: 40px;
}
#white2 {
	background: white;
}
#contentWrapper {
    width: 800px;
    margin: 0 0 0 100px;
    background: #ccc;
}
#headerBar1 {
    width: 100%;
    height: 20px;
    background: black;
}
#headerBar2 {
    width: 100%;
    height: 40px;
    background: black;
}
#logobar {
	padding: 10 10 10 100;
	background: white;
	height: 40px;
}
#footerBar {
    height: 80px;
    background: #be050f;
}
#linksfooter {
	background: #333333;
	color: white;
	text-align: center;
	text-decoration: none;
	min-height: 80px;
	font-size: 150%;
}
#mainbox {
    width: 600px;
	background-color: white;
    padding: 10px;
    border: 2px solid black;
    margin: 20px;
}
a { text-decoration: none; color:white; }
a:visited { text-decoration: none; color:white; }
a:hover { text-decoration: none; color:white; }
a:focus { text-decoration: none; color:white; }
a:hover, a:active { text-decoration: none; color:white; }
</style>

<head>

<?php
$base = $_POST['base'];
$base = "525";
$rpttype = $_POST['rpttype'];
$counts = $_POST['counts'];
$details = $_POST['details'];
$fields = $_POST['fields'];
$extmapping = $_POST['extmapping'];
$totals = "0";


function ceiling($number, $significance) {
if ($significance != null) {
    return (is_numeric($number) && is_numeric($significance) ) ? (ceil($number / $significance) * $significance) : $number;
} else {
    return $number;
}}
?>


<body>
<div id="white2">
<div id="contentWrapper">
<br>
<h2>
Billing Generator
</h2>

<b>Directions:</b><br>
Enter the values below and the form will calculate the fee for building the report.<br>
<br>
Base Fee:  $525
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
Report Type: 
<input type="radio" name="rpttype" <?php if (isset($rpttype) && $rpttype=="0") echo "checked";?> value="0">Columnar
<input type="radio" name="rpttype" <?php if (isset($rpttype) && $rpttype=="175") echo "checked";?> value="175">Data Extract (+$175)
<input type="radio" name="rpttype" <?php if (isset($rpttype) && $rpttype=="350") echo "checked";?> value="350">Fixed Width (+$350)
<br>
Counts/Totals if Data Extract (+$175 each): <input type="number" name="counts" value="<?php echo $counts;?>" required><br>
Details (+$175 each over the first detail): <input type="number" name="details" value="<?php echo $details;?>" required><br>
Fields (+$175 per 100 fields, include header and trailer fields if Data Extract): <input type="number" name="fields" value="<?php echo $fields;?>" required><br>
Extensive Mapping Hours (additional hours for various reasons, +$175 per hour): <input type="number" name="extmapping" value="<?php echo $extmapping;?>" required><br>
<br>
<input type="submit">
</form>	
<br>
<br>	
Check below for the Totals:
<br>
<br>
<div id="mainbox">
<br>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
echo "Build Totals";
echo "<br><br>";
echo "$ ";
echo $base;
echo " Base Fee";
echo "<br><br>";
//if ($rpttype == 0) {
//    echo "$ ";
//    echo $rpttype;
//    echo " Fee for Columnar report";
//    echo "<br><br>";
//}
if ($rpttype == 175) {
    echo "$ ";
    echo $rpttype;
    echo " Fee for Data Extract report";
    echo "<br><br>";
}
if ($rpttype == 350) {
    echo "$ ";
    echo $rpttype;
    echo " Fee for Fixed Width Data Extract report";
    echo "<br><br>";
}
if ($counts > 0) {
    echo "$ ";
    echo $counts * 175;
    echo " Fee for Counts/Totals";
    echo "<br><br>";
}
if ($details > 1) {
    echo "$ ";
    echo ($details - 1) * 175;
    echo " Additional Fee for Details";
    echo "<br><br>";
}
if ($fields > 60) {
    echo "$ ";
    echo ceiling($fields, 100) / 100 * 175;
    echo " Additional Fee for Field Counts";
    echo "<br><br>";
}
if ($extmapping != 0) {
    echo "$ ";
    echo $extmapping * 175;
    echo " Additional Fee for Extensive Mapping";
    echo "<br><br>";
}

//totals
$totals = ($base + $rpttype + ($counts * 175) + (($details - 1) * 175) + ((ceiling($fields, 100) / 100) * 175) + ($extmapping * 175));

echo "$ ";
echo $totals;
echo " TOTAL FEES";
echo "<br><br>";
    
}

?>
</div>
<br>
</div>
</div>


<div id="linksfooter">
	<br>	
        <br>
</div>
</body>
</html> 
